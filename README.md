# gitlab.com/brunomurino/dotfiles

dotfiles, managed with [`chezmoi`](https://github.com/twpayne/chezmoi).

pretty much copied from https://github.com/chimurai/dotfiles

## Install

This'll install Homebrew and chezmoi. After installation `chezmoi` will be initialized.

```shell
/bin/bash -c "$(curl -fsSL https://gitlab.com/brunomurino/dotfiles/-/raw/main/install.sh)"
```

## Initialise chezmoi

Prerequisite: Homebrew & chezmoi

```shell
chezmoi init --apply --force git@gitlab.com:brunomurino/dotfiles.git
```
```shell
chezmoi init --apply --force https://gitlab.com/brunomurino/dotfiles.git
```

## Apps and configs I always use

Font: Fira Code Nerd Font, from https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts
iTerm2 theme: snazzy
