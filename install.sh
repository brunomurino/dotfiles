#!/bin/bash

set -eufo pipefail

echo ""
echo "🤚  This script will setup .dotfiles for you."
read -n 1 -r -s -p $'    Press any key to continue or Ctrl+C to abort...\n\n'


# Install Homebrew
command -v brew >/dev/null 2>&1 || \
  (echo '🍺  Installing Homebrew' && /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)")

eval "$(/opt/homebrew/bin/brew shellenv)"

echo "Verifying Homebrew installation; Running 'brew --version'"

echo ""
brew --version
echo ""

brew install neovim tmux zplug podman

# https://github.com/junegunn/vim-plug#neovim
/bin/bash -c "$(curl -fLo ${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim)"
# sh -c `curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
#        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`

# Install Oh My Zsh
if [ ! -f ~/.oh-my-zsh/oh-my-zsh.sh ]; then
  (echo '💰  Installing oh-my-zsh' && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended)
fi

# Install chezmoi
command -v chezmoi >/dev/null 2>&1 || \
  (echo '👊  Installing chezmoi' && brew install chezmoi)

if [ -d "$HOME/.local/share/chezmoi/.git" ]; then
  echo "🚸  chezmoi already initialized"
  echo "    Reinitialize with: 'eval \"\$(/opt/homebrew/bin/brew shellenv)\" && chezmoi init --apply --force https://gitlab.com/brunomurino/dotfiles.git'"
else
  echo "🚀  Initialize dotfiles with:"
  echo "    eval \"\$(/opt/homebrew/bin/brew shellenv)\" && chezmoi init --apply --force https://gitlab.com/brunomurino/dotfiles.git"
fi

echo ""
echo "Done."
